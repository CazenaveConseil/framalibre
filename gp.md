---
title: Pour le Grand Public
order: 1
in_menu: true
---

Mes recommandations pour un numérique éthique et libre. 

# Premiers pas vers une transition numérique éthique personnelle
## Le plus simple
Utiliser Mozilla Firefox pour naviguer sur le web. 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Firefox.png">
    </div>
    <div>
      <h2>Firefox</h2>
      <p>Le navigateur web épris de liberté distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/firefox.html">Vers la notice Framalibre</a>
        <a href="https://www.mozilla.org/fr/firefox/">Vers le site</a>
      </div>
    </div>
  </article>

Et y ajouter un bloqueur de pub. 
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/uBlock%20Origin.png">
    </div>
    <div>
      <h2>uBlock Origin</h2>
      <p>Une extension vous laissant le choix de bloquer, ou non, les publicités des sites web que vous rencontrez.</p>
      <div>
        <a href="https://framalibre.org/notices/ublock-origin.html">Vers la notice Framalibre</a>
        <a href="https://github.com/gorhill/uBlock">Vers le site</a>
      </div>
    </div>
  </article>

## Le plus sûr
Stocker ses mots de passe dans KeePass. 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/KeePass.png">
    </div>
    <div>
      <h2>KeePass</h2>
      <p>KeePass est un logiciel de gestion des mots de passe.</p>
      <div>
        <a href="https://framalibre.org/notices/keepass.html">Vers la notice Framalibre</a>
        <a href="http://keepass.info/">Vers le site</a>
      </div>
    </div>
  </article>

## Le plus impactant
S'attaquer aux appareils numériques, responsables de la plus grosse part de l'impact environnemental du numérique.
1. Limiter le nombre (oublier Alexia, Google Home ou autres objets connectés).
2. Ne changer que si l'appareil actuel ne fonctionne plus et n'est pas réparable. 
3. Louer plutôt qu'acheter (ex : [Commown](https://commown.coop/)).
4. Préférer du matériel d'occasion/restauré (ex : [La Rebooterie](https://larebooterie.fr/)).
5. Préférer des appareils réparables, durables et éco-conçus (ex: [Fairphone](https://www.fairphone.com/fr/)).
6. Choisir un OS éthique.

### Pour le mobile
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/-e-.png">
    </div>
    <div>
      <h2>/e/</h2>
      <p>Un système d'exploitation pour smartphones respectueux de vos données personnelles.</p>
      <div>
        <a href="https://framalibre.org/notices/e.html">Vers la notice Framalibre</a>
        <a href="https://e.foundation/">Vers le site</a>
      </div>
    </div>
  </article>

### Pour l'ordinateur
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Ubuntu.png">
    </div>
    <div>
      <h2>Ubuntu</h2>
      <p>Système d'exploitation libre facile à installer et à administrer.</p>
      <div>
        <a href="https://framalibre.org/notices/ubuntu.html">Vers la notice Framalibre</a>
        <a href="https://ubuntu.com/">Vers le site</a>
      </div>
    </div>
  </article>


## Le plus privé 
Signal pour discuter avec ses amis. Après tout, vos conversations ne regardent que vous. 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h2>Signal</h2>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article>

## Le plus divertissant
### Regarder ses vidéos en local avec VLC 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/VLC.png">
    </div>
    <div>
      <h2>VLC</h2>
      <p>VLC est un lecteur multimédia très populaire.</p>
      <div>
        <a href="https://framalibre.org/notices/vlc.html">Vers la notice Framalibre</a>
        <a href="https://www.videolan.org/vlc/">Vers le site</a>
      </div>
    </div>
  </article>

### Regarder les vidéos en ligne sur peerTube.
<article class="framalibre-notice">
  <div>
    <img src="https://beta.framalibre.org/images/logo/PeerTube.png">
  </div>
  <div>
    <h2>PeerTube</h2>
    <p>PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos.</p>
    <div>
      <a href="https://beta.framalibre.org/notices/peertube.html">Vers la notice Framalibre</a>
      <a href="https://joinpeertube.org/fr/">Vers le site</a>
    </div>
  </div>
</article>

## Le plus low tech 
Éteindre ses appareils, se déplacer et aller à la rencontre des gens :) 
