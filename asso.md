---
title: "Pour une (petite) association"
order: 2
in_menu: true
---
Mes recommandations pour un usage numérique éthique et libre dans votre association.

# Pour collaborer
## Stocker et partager des documents
Utiliser une instance Nextcloud, une bonne alternative à Google Drive ou Dropbox.

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article>

Les associations peuvent obtenir gratuitement une instance de 40Go de stockage avec [Framaspace](https://www.frama.space/abc/fr/).

## Ecrire à plusieurs
Pour prendre des notes à plusieurs, Etherpad est très pratique. 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Etherpad.png">
    </div>
    <div>
      <h2>Etherpad</h2>
      <p>Un éditeur de texte collaboratif et en temps réel !</p>
      <div>
        <a href="https://framalibre.org/notices/etherpad.html">Vers la notice Framalibre</a>
        <a href="http://etherpad.org/">Vers le site</a>
      </div>
    </div>
  </article>

Exemple d'instance : [Pad](https://pad.le-filament.com/) proposé gratuitement par la SCOP Le Filament.


## Choisir une date
Pour réaliser un sondage, utiliser Framadate, l'alternative libre à Doodle.

  <article class="framalibre-notice">
    <div>
      <img src="https://framadate.org/abc/img/icons/date.svg">
    </div>
    <div>
      <h2>Framadate</h2>
      <p>Organiser des rendez-vous simplement, librement.</p>
      <div>
        <a href="https://framalibre.org/notices/framadate.html">Vers la notice Framalibre</a>
        <a href="https://framadate.org/">Vers le site</a>
      </div>
    </div>
  </article>

# Pour le suivi de projet
## KANBAN
Wekan est une bonne alternative à Trello.

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Wekan.png">
    </div>
    <div>
      <h2>Wekan</h2>
      <p>Un gestionnaire de tâches collaboratif riche en fonctionnalités !</p>
      <div>
        <a href="https://framalibre.org/notices/wekan.html">Vers la notice Framalibre</a>
        <a href="https://wekan.github.io/">Vers le site</a>
      </div>
    </div>
  </article>
  
Exemple d'instance : [Pikan](https://kanban.picasoft.net/sign-in) hebergé par l'association Picasoft.

# Pour communiquer
## Messagerie instantanée
Signal, la meilleure alternative à Whatsapp

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h2>Signal</h2>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article>

## Visioconférence
Plutôt que Zoom, préférer une alternative libre comme Jitsi.
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Jitsi.png">
    </div>
    <div>
      <h2>Jitsi</h2>
      <p>Visio-conférence, messagerie instantanée et plus à essayer sur meet.jit.si.</p>
      <div>
        <a href="https://framalibre.org/notices/jitsi.html">Vers la notice Framalibre</a>
        <a href="https://jitsi.org/">Vers le site</a>
      </div>
    </div>
  </article>

Exemple d'instance : [kMeet](https://kmeet.infomaniak.com/) proposé gratuitement et sans compte par Infomaniak.

## e-Mail
ProtonMail plutôt que Gmail.

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/ProtonMail.png">
    </div>
    <div>
      <h2>ProtonMail</h2>
      <p>ProtonMail est un service de messagerie sécurisé respectant votre vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/protonmail.html">Vers la notice Framalibre</a>
        <a href="https://proton.me/fr/mail">Vers le site</a>
      </div>
    </div>
  </article>

## Organisation d'événement 

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Mobilizon.png">
    </div>
    <div>
      <h2>Mobilizon</h2>
      <p>Un outil libre et fédéré pour se rassembler, s'organiser, se mobiliser et agir.</p>
      <div>
        <a href="https://framalibre.org/notices/mobilizon.html">Vers la notice Framalibre</a>
        <a href="https://joinmobilizon.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>

Il existe une multitudes d'autres outils libres. 

### N'hésitez pas à me contacter à l'adresse arnaud@cazenaveconseil.fr si vous ne trouvez pas l'outil qui répond à vos besoins.
