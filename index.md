---
title: Recommandations de Cazenave Conseil
order: 0
---

Mes recommandations pour un numérique éthique et libre. 

# Pour le grand public
[Recommandations grand public](https://framalibre.cazenaveconseil.fr/gp.html)

# Pour une (petite) association 
[Recommandations association](https://framalibre.cazenaveconseil.fr/asso.html)

# Pour des besoins plus spécifiques
Vous trouverez sans doute l'outil qu'il vous faut sur [l'annuaire Framalibre](https://framalibre.org/).

### Si vous avez besoin d'aide ou de conseils, n'hésitez pas à me contacter à l'adresse arnaud@cazenaveconseil.fr
